# Frontend Mentor Challenge Solutions

This is the collection of all my Frontend Mentor challenge solutions. You can find links to their repositories, live demos and direct links to challenges on Frontend Mentor so you can try them out yourself. Follow the instcutions below in case you decide to download or fork the project. I am using Vue so you will need additonal node modules to run the project locally.

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

## Project List

### URL shortening API landing page

<a href="https://catherineisonline.github.io/url-shortening-api-frontendmentor/">Live</a> | <a href="https://github.com/catherineisonline/url-shortening-api-frontendmentor">Repo</a> | <a href="https://www.frontendmentor.io/challenges/url-shortening-api-landing-page-2ce3ob-G">Try Out</a>

### IP Address Tracker

<a href="https://catherineisonline.github.io/ip-address-tracker-frontendmentor/">Live</a> | <a href="https://github.com/catherineisonline/ip-address-tracker-frontendmentor">Repo</a> | <a href="https://www.frontendmentor.io/challenges/ip-address-tracker-I8-0yYAH0">Try Out</a>

### Testimonials grid section

<a href="https://catherineisonline.github.io/testimonials-grid-section-frontendmentor/">Live</a> | <a href="https://github.com/catherineisonline/testimonials-grid-section-frontendmentor">Repo</a> | <a href="https://www.frontendmentor.io/challenges/testimonials-grid-section-Nnw6J7Un7">Try Out</a>

### QR code component

<a href="https://catherineisonline.github.io/QR-code-component-frontendmentor/">Live</a> | <a href="https://github.com/catherineisonline/QR-code-component-frontendmentor">Repo</a> | <a href="https://www.frontendmentor.io/challenges/qr-code-component-iux_sIO_H">Try Out</a>

### Fylo data storage component

<a href="https://catherineisonline.github.io/fylo-data-storage-component-frontendmentor/">Live</a> | <a href="https://github.com/catherineisonline/fylo-data-storage-component-frontendmentor">Repo</a> | <a href="https://www.frontendmentor.io/challenges/fylo-data-storage-component-1dZPRbV5n">Try Out</a>

### Huddle landing page with a single introductory section

<a href="https://catherineisonline.github.io/huddle-landing-page-with-a-single-introductory-section-frontendmentor/">Live</a> | <a href="https://github.com/catherineisonline/huddle-landing-page-with-a-single-introductory-section-frontendmentor">Repo</a> | <a href="https://www.frontendmentor.io/challenges/huddle-landing-page-with-a-single-introductory-section-B_2Wvxgi0">Try Out</a>

### NFT preview card component

<a href="https://catherineisonline.github.io/nft-preview-card-frontendmentor/">Live</a> | <a href="https://github.com/catherineisonline/nft-preview-card-frontendmentor">Repo</a> | <a href="https://www.frontendmentor.io/challenges/nft-preview-card-component-SbdUL_w0U">Try Out</a>

### Four card feature section

<a href="https://catherineisonline.github.io/four-card-feature-section-frontendmentor/">Live</a> | <a href="https://github.com/catherineisonline/four-card-feature-section-frontendmentor">Repo</a> | <a href="https://www.frontendmentor.io/challenges/four-card-feature-section-weK1eFYK">Try Out</a>

### Social proof section

<a href="https://catherineisonline.github.io/social-proof-section-frontendmentor/">Live</a> | <a href="https://github.com/catherineisonline/social-proof-section-frontendmentor">Repo</a> | <a href="https://www.frontendmentor.io/challenges/social-proof-section-6e0qTv_bA">Try Out</a>

### 3-column preview card component

<a href="https://catherineisonline.github.io/3-column-card-component-frontendmentor/">Live</a> | <a href="https://github.com/catherineisonline/3-column-card-component-frontendmentor">Repo</a> | <a href="https://www.frontendmentor.io/challenges/3column-preview-card-component-pH92eAR2-">Try Out</a>

### Single price grid component

<a href="https://catherineisonline.github.io/single-price-grid-component-frontendmentor/">Live</a> | <a href="https://github.com/catherineisonline/single-price-grid-component-frontendmentor">Repo</a> | <a href="https://www.frontendmentor.io/challenges/single-price-grid-component-5ce41129d0ff452fec5abbbc">Try Out</a>

### Profile card component

<a href="https://catherineisonline.github.io/profile-card-component-frontendmentor/">Live</a> | <a href="https://github.com/catherineisonline/profile-card-component-frontendmentor">Repo</a> | <a href="https://www.frontendmentor.io/challenges/profile-card-component-cfArpWshJ">Try Out</a>

### Order summary component

<a href="https://catherineisonline.github.io/order-summary-component-frontendmentor/">Live</a> | <a href="https://github.com/catherineisonline/order-summary-component-frontendmentor">Repo</a> | <a href="https://www.frontendmentor.io/challenges/order-summary-component-QlPmajDUj">Try Out</a>

### Stats preview card component

<a href="https://catherineisonline.github.io/stats-preview-card-component-frontendmentor/">Live</a> | <a href="https://github.com/catherineisonline/stats-preview-card-component-frontendmentor">Repo</a> | <a href="https://www.frontendmentor.io/challenges/stats-preview-card-component-8JqbgoU62">Try Out</a>
